// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "SnakeBody.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Wall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	Wall->SetCollisionResponseToAllChannels(ECR_Overlap);
	Wall->SetWorldScale3D(FVector(0.5, 0.5, 0.5));
}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::Interact(AActor* Interactor, bool bIsFirst)
{
	auto Snake = Cast<ASnakeBody>(Interactor);
	if (IsValid(Snake)) {
		Snake->Destroy();
	}
}

