// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBody.generated.h"

class ASnakeBodyPiece;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ASnakeBody : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBody();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBodyPiece> SnakeBodyPieceClass;

	UPROPERTY(EditDefaultsOnly)
		float PieceSize;

	UPROPERTY()
		TArray<ASnakeBodyPiece*> Pieces;

	UPROPERTY(EditDefaultsOnly)
		int CuantityOfPiecesOnStart = 3;

	UPROPERTY()
		EMovementDirection CurrentDirection;
	UPROPERTY()
		EMovementDirection NextDirection;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UFUNCTION()
		void OnSnakePieceOverlap(ASnakeBodyPiece* OverlappedPiece, AActor* OtherActor);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakePieceOnStart(int NumberOfPieces = 1);
	void AddSnakePiece(FVector SpawnLocation = FVector(0, 0, -100));

	void Move();
};
