// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePawn.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBody.h"
#include "Components/InputComponent.h"

// Sets default values
ASnakePawn::ASnakePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	PawnCamera->SetRelativeRotation(FRotator(-90,0,0));

}

// Called when the game starts or when spawned
void ASnakePawn::BeginPlay()
{
	Super::BeginPlay();
	CreateSnakeActor();
	
}

// Called every frame
void ASnakePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASnakePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("VerticalUp"), this, &ASnakePawn::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis(TEXT("HorizontalRight"), this, &ASnakePawn::HandlePlayerHorizontalInput);
}

void ASnakePawn::CreateSnakeActor()
{
	SnakeBody = GetWorld()->SpawnActor<ASnakeBody>(SnakeBodyClass, FTransform());
}

void ASnakePawn::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeBody)) {
		EMovementDirection& CurrentDiraction = SnakeBody->CurrentDirection;
		EMovementDirection& NextDiraction = SnakeBody->NextDirection;

		if (CurrentDiraction != EMovementDirection::UP && CurrentDiraction != EMovementDirection::DOWN)		//FacePumProtection
			if (value > 0) 
				NextDiraction = EMovementDirection::UP;
			else if (value < 0)
				NextDiraction = EMovementDirection::DOWN;
	}
}

void ASnakePawn::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeBody)) {
		EMovementDirection& CurrentDiraction = SnakeBody->CurrentDirection;
		EMovementDirection& NextDiraction = SnakeBody->NextDirection;

		if (CurrentDiraction != EMovementDirection::RIGHT && CurrentDiraction != EMovementDirection::LEFT)	//FacePumProtection
			if (value > 0)
				NextDiraction = EMovementDirection::RIGHT;
			else if (value < 0)
				NextDiraction = EMovementDirection::LEFT;
	}
}

