// Fill out your copyright notice in the Description page of Project Settings.



#include "SnakeBodyPiece.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBody.h"


// Sets default values
ASnakeBodyPiece::ASnakeBodyPiece()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	Piece = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
//	RootComponent = Piece;

	Piece->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeBodyPiece::BeginPlay()
{
	Super::BeginPlay();
	Piece->OnComponentBeginOverlap.AddDynamic(this, &ASnakeBodyPiece::HandleBeginOverlap);
}

// Called every frame
void ASnakeBodyPiece::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeBodyPiece::SetAsFirst_Implementation()
{
	bIsFirst = true;
}

void ASnakeBodyPiece::Interact(AActor* Interactor, bool bIsHead)
{
	if (IsValid(SnakeBodyRef)&&bIsHead) {
		SnakeBodyRef->Destroy();
	}
}

void ASnakeBodyPiece::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
										AActor* OtherActor,
										UPrimitiveComponent* OtherComponent,
										int32 OtherBodyIndex, bool bFromSweep,
										const FHitResult& SweepResult)
{
	if (IsValid(SnakeBodyRef)) {
		SnakeBodyRef->OnSnakePieceOverlap(this, OtherActor);
	}
}

void ASnakeBodyPiece::ToggleCollision()
{
	if (Piece->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
		Piece->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	else
		Piece->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

