// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBody.h"
#include "SnakeBodyPiece.h"
#include "Interactable.h"

// Sets default values
ASnakeBody::ASnakeBody()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PieceSize = 50.f;
	MovementSpeed = 0.5f;
	CurrentDirection = EMovementDirection::UP;
	NextDirection = EMovementDirection::UP;
}

void ASnakeBody::OnSnakePieceOverlap(ASnakeBodyPiece* OverlappedPiece, AActor* OtherActor)
{
	if (IsValid(OverlappedPiece)) {
		IInteractable* Interface = Cast<IInteractable>(OtherActor);
		if (Interface) {
			Interface->Interact(this, OverlappedPiece->bIsFirst);
		}
	}
}

// Called when the game starts or when spawned
void ASnakeBody::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakePieceOnStart(CuantityOfPiecesOnStart);
}

// Called every frame
void ASnakeBody::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBody::AddSnakePieceOnStart(int NumberOfPieces)
{
	while (NumberOfPieces-- > 0) {
		ASnakeBodyPiece* NewPiece = GetWorld()->SpawnActor<ASnakeBodyPiece>(SnakeBodyPieceClass, FTransform(GetActorLocation() - FVector(Pieces.Num() * PieceSize, 0, 0)));
		int32 NewPieceNumber = Pieces.Add(NewPiece);
		if (NewPieceNumber == 0)
			NewPiece->SetAsFirst();
		NewPiece->SnakeBodyRef = this;
	}
}

void ASnakeBody::AddSnakePiece(FVector SpawnLocation)
{
	ASnakeBodyPiece* NewPiece = GetWorld()->SpawnActor<ASnakeBodyPiece>(SnakeBodyPieceClass, FTransform(SpawnLocation));
	int32 NewPieceNumber = Pieces.Add(NewPiece);
	NewPiece->SnakeBodyRef = this;

}

void ASnakeBody::Move()
{
	Pieces[0]->ToggleCollision();
	CurrentDirection = NextDirection;
	for (int i = Pieces.Num() - 1; i > 0;) {
		auto CurrentPiece = Pieces[i];
		auto NextPiece = Pieces[--i];
		FVector NewLocation = NextPiece->GetActorLocation();
		CurrentPiece->SetActorLocation(NewLocation);
	}

	FVector MovementVector(0,0,0);
	switch (CurrentDirection){
	case EMovementDirection::UP:
		MovementVector.X = PieceSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y = -PieceSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X = -PieceSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y = PieceSize;
		break;
	}

	Pieces[0]->AddActorWorldOffset(MovementVector);
	Pieces[0]->ToggleCollision();
}

