// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBodyPiece.generated.h"

class UStaticMeshComponent;
class ASnakeBody;

UCLASS()
class SNAKE_API ASnakeBodyPiece : public AActor, public IInteractable
{
	GENERATED_BODY()
public:	
	// Sets default values for this actor's properties
	ASnakeBodyPiece();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		bool bIsFirst = false;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* Piece;

	UPROPERTY()
		ASnakeBody* SnakeBodyRef = nullptr;

//	UPROPERTY(EditDefaultsOnly)
//		TSubclassOf<UStaticMeshComponent> PieceMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
		void SetAsFirst();
	void SetAsFirst_Implementation();

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
								AActor* OtherActor,
								UPrimitiveComponent* OtherComponent, 
								int32 OtherBodyIndex, bool bFromSweep, 
								const FHitResult &SweepResult);

	UFUNCTION()
		void ToggleCollision();
};
